#Author: Daniela Kupper
#Date: 18.08.2021
#Title: Plots of candidate genes - QZ dataset (file name:fpkm_genename)

setwd()
data<-read.csv(file="fpkm_genename.csv", sep=";", header=TRUE)

#install.packages("tidyverse")
library("tidyverse")
#install.packages("tibble")
library("tibble")



#select genes of interest 
mydata<-data %>%
  filter(gene_id %in% c("PGSC0003DMT400063324", "PGSC0003DMG400037578", "PGSC0003DMG400008307","PGSC0003DMG400021136","PGSC0003DMG400021157",
                        "PGSC0003DMG400021142", "PGSC0003DMG400021152", "PGSC0003DMG400021216", "PGSC0003DMG400021175", "PGSC0003DMG400004501", 
                        "PGSC0003DMG400006942", "PGSC0003DMG400028459","PGSC0003DMG400033148", "PGSC0003DMG400004895", "PGSC0003DMG400010964", 
                        "PGSC0003DMG400010484", "PGSC0003DMG400018249", "PGSC0003DMG400024630" ))
view(mydata)

############### get a tidy table ####################################################

#M_3dpi gathering
M_3dpi<-gather(mydata, key="replicate",value = "measurement" ,M_3dpi_1, M_3dpi_2, M_3dpi_3)
M_3dpi<-M_3dpi %>% select(c("replicate", "measurement", "gene_id"))
M_3dpi$name<-"M_3dpi"
M_3dpi$timepoint<-3
M_3dpi$treatment<-"M"
M_3dpi<-M_3dpi %>% select(c("name", "timepoint", "treatment", "measurement", "gene_id", "replicate"))
M_3dpi

#R_3dpi gathering
R_3dpi<-gather(mydata, key="replicate",value = "measurement" ,R_3dpi_1, R_3dpi_2, R_3dpi_3)
R_3dpi<-R_3dpi %>% select(c("replicate", "measurement", "gene_id"))
R_3dpi$treatment<-"R"
R_3dpi$name<-"R_3dpi"
R_3dpi$timepoint<-3
R_3dpi<-R_3dpi %>% select(c("name", "timepoint", "treatment", "measurement", "gene_id", "replicate"))
R_3dpi
newdata<-merge(M_3dpi, R_3dpi, all.x=TRUE, all.y = TRUE)#merge with previous table 

#D_3dpi gathering
D_3dpi<-gather(mydata, key="replicate",value = "measurement" ,D_3dpi_1, D_3dpi_2, D_3dpi_3)
D_3dpi<-D_3dpi %>% select(c("replicate", "measurement", "gene_id"))
D_3dpi$treatment<-"D"
D_3dpi$name<-"D_3dpi"
D_3dpi$timepoint<-3
D_3dpi<-D_3dpi %>% select(c("name", "timepoint", "treatment", "measurement", "gene_id","replicate"))
D_3dpi
newdata<-merge(newdata, D_3dpi, all.x=TRUE, all.y = TRUE)#merge with previous table 

#M_6dpi gathering
M_6dpi<-gather(mydata, key="replicate",value = "measurement" , M_6dpi_1, M_6dpi_2, M_6dpi_3)
M_6dpi<-M_6dpi %>% select(c("replicate", "measurement", "gene_id"))
M_6dpi$treatment<-"M"
M_6dpi$name<-"M_6dpi"
M_6dpi$timepoint<-6
M_6dpi<-M_6dpi %>% select(c("name", "timepoint", "treatment", "measurement", "gene_id", "replicate"))
M_6dpi
newdata<-merge(newdata, M_6dpi, all.x=TRUE, all.y = TRUE)#merge with previous table 

#R_6dpi gathering
R_6dpi<-gather(mydata, key="replicate",value = "measurement" ,R_6dpi_1, R_6dpi_2, R_6dpi_3)
R_6dpi<-R_3dpi %>% select(c("replicate", "measurement", "gene_id"))
R_6dpi$treatment<-"R"
R_6dpi$name<-"R_6dpi"
R_6dpi$timepoint<-6
R_6dpi<-R_6dpi %>%  select(c("name", "timepoint", "treatment", "measurement", "gene_id", "replicate"))
R_6dpi
newdata<-merge(newdata, R_6dpi, all.x=TRUE, all.y = TRUE)#merge with previous table 

#D_6dpi gathering
D_6dpi<-gather(mydata, key="replicate",value = "measurement" ,D_6dpi_1, D_6dpi_2, D_6dpi_3)
D_6dpi<-D_6dpi %>% select(c("replicate", "measurement", "gene_id"))
D_6dpi$treatment<-"D"
D_6dpi$name<-"D_6dpi"
D_6dpi$timepoint<-6
D_6dpi<-D_6dpi %>% select(c("name", "timepoint", "treatment", "measurement", "gene_id", "replicate"))
D_6dpi
newdata<-merge(newdata, D_6dpi, all.x=TRUE, all.y = TRUE)#merge with previous table 
newdata <-newdata%>%select(c("name", "timepoint", "treatment", "measurement", "gene_id", "replicate"))

#add column batch based on replicate 
newdata<- newdata %>%
  mutate(batch = case_when(
    endsWith(replicate, "1") ~ 1,
    endsWith(replicate, "2") ~ 2,
    endsWith(replicate, "3") ~ 3
  ))

#view new table 
view(newdata)

############## plots of gene of interest ##############################################################

#save all plots in one pdf

pdf("qi_dataset_plots_add_18genes.pdf", width = 8, height = 8)

#PGSC0003DMG400004501
g1<- newdata%>%
  filter(gene_id %in% c("PGSC0003DMG400004501"))

ggplot(g1, aes(name, measurement, 
               colour=treatment, shape=factor(batch)))+
  geom_point(size=3.5)+
  labs(title="Gene: PGSC0003DMG400004501", y="Fragments Per Kilobase of transcript per Million mapped reads", x="" )+
  theme_grey()+
  scale_x_discrete(limits=c("D_3dpi", "M_3dpi", "R_3dpi","D_6dpi", "M_6dpi", "R_6dpi"))

#PGSC0003DMG400037578
g2<- newdata%>%
  filter(gene_id %in% c("PGSC0003DMG400037578"))

ggplot(g2, aes(name, measurement, 
               colour=treatment,shape=factor(batch)))+
  geom_point(size=3.5)+
  labs(title="Gene: PGSC0003DMG400037578", y="Fragments Per Kilobase of transcript per Million mapped reads", x="" )+
  theme_grey()+
  scale_x_discrete(limits=c("D_3dpi", "M_3dpi", "R_3dpi","D_6dpi", "M_6dpi", "R_6dpi"))

#PGSC0003DMG400008307
g3<- newdata%>%
  filter(gene_id %in% c("PGSC0003DMG400008307"))

ggplot(g3, aes(name, measurement, 
               colour=treatment, shape=factor(batch)))+
  geom_point(size=3.5)+
  labs(title="Gene: PGSC0003DMG400008307", y="Fragments Per Kilobase of transcript per Million mapped reads", x="" )+
  theme_grey()+
  scale_x_discrete(limits=c("D_3dpi", "M_3dpi", "R_3dpi","D_6dpi", "M_6dpi", "R_6dpi"))

#PGSC0003DMG400021136
g4<- newdata%>%
  filter(gene_id %in% c("PGSC0003DMG400021136"))

ggplot(g4, aes(name, measurement, 
               colour=treatment,shape=factor(batch)))+
  geom_point(size=3.5)+
  labs(title="Gene: PGSC0003DMG400021136", y="Fragments Per Kilobase of transcript per Million mapped reads", x="" )+
  theme_grey()+
  scale_x_discrete(limits=c("D_3dpi", "M_3dpi", "R_3dpi","D_6dpi", "M_6dpi", "R_6dpi"))

#PGSC0003DMG400021157
g5<- newdata%>%
  filter(gene_id %in% c("PGSC0003DMG400021157"))

ggplot(g5, aes(name, measurement, 
               colour=treatment,shape=factor(batch)))+
  geom_point(size=3.5)+
  labs(title="Gene: PGSC0003DMG400021157", y="Fragments Per Kilobase of transcript per Million mapped reads", x="" )+
  theme_grey()+
  scale_x_discrete(limits=c("D_3dpi", "M_3dpi", "R_3dpi","D_6dpi", "M_6dpi", "R_6dpi"))

#PGSC0003DMG400021142
g6<- newdata%>%
  filter(gene_id %in% c("PGSC0003DMG400021142"))

ggplot(g6, aes(name, measurement, 
               colour=treatment,shape=factor(batch)))+
  geom_point(size=3.5)+
  labs(title="Gene: PGSC0003DMG400021142", y="Fragments Per Kilobase of transcript per Million mapped reads", x="" )+
  theme_grey()+
  scale_x_discrete(limits=c("D_3dpi", "M_3dpi", "R_3dpi","D_6dpi", "M_6dpi", "R_6dpi"))

#PGSC0003DMG400021152
g7<- newdata%>%
  filter(gene_id %in% c("PGSC0003DMG400021152"))

ggplot(g7, aes(name, measurement, 
               colour=treatment,shape=factor(batch)))+
  geom_point(size=3.5)+
  labs(title="Gene: PGSC0003DMG400021152", y="Fragments Per Kilobase of transcript per Million mapped reads", x="" )+
  theme_grey()+
  scale_x_discrete(limits=c("D_3dpi", "M_3dpi", "R_3dpi","D_6dpi", "M_6dpi", "R_6dpi"))

#PGSC0003DMG400021216
g8<- newdata%>%
  filter(gene_id %in% c("PGSC0003DMG400021216"))

ggplot(g8, aes(name, measurement, 
               colour=treatment,shape=factor(batch)))+
  geom_point(size=3.5)+
  labs(title="Gene: PGSC0003DMG400021216", y="Fragments Per Kilobase of transcript per Million mapped reads", x="" )+
  theme_grey()+
  scale_x_discrete(limits=c("D_3dpi", "M_3dpi", "R_3dpi","D_6dpi", "M_6dpi", "R_6dpi"))

#PGSC0003DMG400021175
g9<- newdata%>%
  filter(gene_id %in% c("PGSC0003DMG400021175"))

ggplot(g9, aes(name, measurement, 
               colour=treatment,shape=factor(batch)))+
  geom_point(size=3.5)+
  labs(title="Gene: PGSC0003DMG400021175", y="Fragments Per Kilobase of transcript per Million mapped reads", x="" )+
  theme_grey()+
  scale_x_discrete(limits=c("D_3dpi", "M_3dpi", "R_3dpi","D_6dpi", "M_6dpi", "R_6dpi"))

#PGSC0003DMG400004501
g10<- newdata%>%
  filter(gene_id %in% c("PGSC0003DMG400004501"))

ggplot(g10, aes(name, measurement, 
               colour=treatment,shape=factor(batch)))+
  geom_point(size=3.5)+
  labs(title="Gene: PGSC0003DMG400004501", y="Fragments Per Kilobase of transcript per Million mapped reads", x="" )+
  theme_grey()+
  scale_x_discrete(limits=c("D_3dpi", "M_3dpi", "R_3dpi","D_6dpi", "M_6dpi", "R_6dpi"))

#PGSC0003DMG400006942
g11<- newdata%>%
  filter(gene_id %in% c("PGSC0003DMG400006942"))

ggplot(g11, aes(name, measurement, 
                colour=treatment,shape=factor(batch)))+
  geom_point(size=3.5)+
  labs(title="Gene: PGSC0003DMG400006942", y="Fragments Per Kilobase of transcript per Million mapped reads", x="" )+
  theme_grey()+
  scale_x_discrete(limits=c("D_3dpi", "M_3dpi", "R_3dpi","D_6dpi", "M_6dpi", "R_6dpi"))

#PGSC0003DMG400028459
g12<- newdata%>%
  filter(gene_id %in% c("PGSC0003DMG400028459"))

ggplot(g12, aes(name, measurement, 
                colour=treatment,shape=factor(batch)))+
  geom_point(size=3.5)+
  labs(title="Gene: PGSC0003DMG400028459", y="Fragments Per Kilobase of transcript per Million mapped reads", x="" )+
  theme_grey()+
  scale_x_discrete(limits=c("D_3dpi", "M_3dpi", "R_3dpi","D_6dpi", "M_6dpi", "R_6dpi"))

#PGSC0003DMG400033148
g13<- newdata%>%
  filter(gene_id %in% c("PGSC0003DMG400033148"))

ggplot(g13, aes(name, measurement, 
                colour=treatment,shape=factor(batch)))+
  geom_point(size=3.5)+
  labs(title="Gene: PGSC0003DMG400033148", y="Fragments Per Kilobase of transcript per Million mapped reads", x="" )+
  theme_grey()+
  scale_x_discrete(limits=c("D_3dpi", "M_3dpi", "R_3dpi","D_6dpi", "M_6dpi", "R_6dpi"))

#PGSC0003DMG400004895
g14<- newdata%>%
  filter(gene_id %in% c("PGSC0003DMG400004895"))

ggplot(g14, aes(name, measurement, 
                colour=treatment,shape=factor(batch)))+
  geom_point(size=3.5)+
  labs(title="Gene: PGSC0003DMG400004895", y="Fragments Per Kilobase of transcript per Million mapped reads", x="" )+
  theme_grey()+
  scale_x_discrete(limits=c("D_3dpi", "M_3dpi", "R_3dpi","D_6dpi", "M_6dpi", "R_6dpi"))

#PGSC0003DMG400010964
g15<- newdata%>%
  filter(gene_id %in% c("PGSC0003DMG400010964"))

ggplot(g15, aes(name, measurement, 
                colour=treatment,shape=factor(batch)))+
  geom_point(size=3.5)+
  labs(title="Gene: PGSC0003DMG400010964", y="Fragments Per Kilobase of transcript per Million mapped reads", x="" )+
  theme_grey()+
  scale_x_discrete(limits=c("D_3dpi", "M_3dpi", "R_3dpi","D_6dpi", "M_6dpi", "R_6dpi"))

#PGSC0003DMG400010484
g16<- newdata%>%
  filter(gene_id %in% c("PGSC0003DMG400010484"))

ggplot(g16, aes(name, measurement, 
                colour=treatment,shape=factor(batch)))+
  geom_point(size=3.5)+
  labs(title="Gene: PGSC0003DMG400010484", y="Fragments Per Kilobase of transcript per Million mapped reads", x="" )+
  theme_grey()+
  scale_x_discrete(limits=c("D_3dpi", "M_3dpi", "R_3dpi","D_6dpi", "M_6dpi", "R_6dpi"))

#PGSC0003DMG400018249
g17<- newdata%>%
  filter(gene_id %in% c("PGSC0003DMG400018249"))

ggplot(g17, aes(name, measurement, 
                colour=treatment,shape=factor(batch)))+
  geom_point(size=3.5)+
  labs(title="Gene: PGSC0003DMG400018249", y="Fragments Per Kilobase of transcript per Million mapped reads", x="" )+
  theme_grey()+
  scale_x_discrete(limits=c("D_3dpi", "M_3dpi", "R_3dpi","D_6dpi", "M_6dpi", "R_6dpi"))

#PGSC0003DMG400024630
g18<- newdata%>%
  filter(gene_id %in% c("PGSC0003DMG400024630"))

ggplot(g18, aes(name, measurement, 
                colour=treatment,shape=factor(batch)))+
  geom_point(size=3.5)+
  labs(title="Gene: PGSC0003DMG400024630", y="Fragments Per Kilobase of transcript per Million mapped reads", x="" )+
  theme_grey()+
  scale_x_discrete(limits=c("D_3dpi", "M_3dpi", "R_3dpi","D_6dpi", "M_6dpi", "R_6dpi"))


#clear after save as pdf
dev.off()










#other ideas to create a tidy table (let theme here in case I need to look at them)

########## create a tidy table #############################################################################################################

#M_3dpi gathering
M_3dpi<-gather(mydata, key="replicate",value = "M_3dpi" ,M_3dpi_1, M_3dpi_2, M_3dpi_3)
M_3dpi<-M_3dpi %>% select(c("M_3dpi", "gene_id"))
M_3dpi

#R_3dpi gathering
R_3dpi<-gather(mydata, key="replicate",value = "R_3dpi" ,R_3dpi_1, R_3dpi_2, R_3dpi_3)
R_3dpi<-R_3dpi %>% select(c("R_3dpi", "gene_id"))
R_3dpi
newdata<-inner_join(M_3dpi, R_3dpi,by= "gene_id")#merge with previous table 

#D_3dpi gathering
D_3dpi<-gather(mydata, key="replicate",value = "D_3dpi" ,D_3dpi_1, D_3dpi_2, D_3dpi_3)
D_3dpi<-D_3dpi %>% select(c("D_3dpi", "gene_id"))
D_3dpi
newdata<-inner_join(newdata, D_3dpi, by= "gene_id")#merge with previous table 

#M_6dpi gathering
M_6dpi<-gather(mydata, key="replicate",value = "M_6dpi" ,M_6dpi_1, M_6dpi_2, M_6dpi_3)
M_6dpi<-M_6dpi %>% select(c("M_6dpi", "gene_id"))
M_6dpi
newdata<-inner_join(newdata, M_6dpi,by= "gene_id")#merge with previous table 

#R_6dpi gathering
R_6dpi<-gather(mydata, key="replicate",value = "R_6dpi" ,R_6dpi_1, R_6dpi_2, R_6dpi_3)
R_6dpi<-R_6dpi %>% select(c("R_6dpi", "gene_id"))
R_6dpi
newdata<-inner_join(newdata, R_6dpi, by= "gene_id")#merge with previous table 

#D_6dpi gathering
D_6dpi<-gather(mydata, key="replicate",value = "D_6dpi" ,D_6dpi_1, D_6dpi_2, D_6dpi_3)
D_6dpi<-D_6dpi %>% select(c("D_6dpi", "gene_id"))
D_3dpi
newdata<-inner_join(newdata,  D_6dpi,by= "gene_id")#merge with previous table 


view(newdata)

#########plots#########

#PGSC0003DMT400063324
newdata%>%
  filter(gene_id %in% c("PGSC0003DMG400004501"))
ggplot(aes(gene_id, ) )

##other try to create a tidy table ############################################################

#M_3dpi gathering
M_3dpi<-gather(mydata, key="replicate",value = "M_3dpi" ,M_3dpi_1, M_3dpi_2, M_3dpi_3)
M_3dpi<-M_3dpi %>% select(c("replicate", "M_3dpi", "gene_id"))
M_3dpi
view(M_3dpi)

#R_3dpi gathering
R_3dpi<-gather(mydata, key="replicate",value = "R_3dpi" ,R_3dpi_1, R_3dpi_2, R_3dpi_3)
R_3dpi<-R_3dpi %>% select(c("replicate", "R_3dpi", "gene_id"))
R_3dpi
view(R_3dpi)
newdata<-merge(M_3dpi, R_3dpi, all.x=TRUE, all.y = TRUE)#merge with previous table 

#D_3dpi gathering
D_3dpi<-gather(mydata, key="replicate",value = "D_3dpi" ,D_3dpi_1, D_3dpi_2, D_3dpi_3)
D_3dpi<-D_3dpi %>% select(c("replicate", "D_3dpi", "gene_id"))
D_3dpi
newdata<-merge(newdata, D_3dpi, all.x=TRUE, all.y = TRUE)#merge with previous table 

#M_6dpi gathering
M_6dpi<-gather(mydata, key="replicate",value = "M_6dpi" ,M_6dpi_1, M_6dpi_2, M_6dpi_3)
M_6dpi<-M_6dpi %>% select(c("replicate", "M_6dpi", "gene_id"))
M_6dpi
newdata<-merge(newdata, M_6dpi, all.x=TRUE, all.y = TRUE)#merge with previous table 

#R_6dpi gathering
R_6dpi<-gather(mydata, key="replicate",value = "R_6dpi" ,R_6dpi_1, R_6dpi_2, R_6dpi_3)
R_6dpi<-R_6dpi %>% select(c("replicate", "R_6dpi", "gene_id"))
R_6dpi
newdata<-merge(newdata, R_6dpi, all.x=TRUE, all.y = TRUE)#merge with previous table 

#D_6dpi gathering
D_6dpi<-gather(mydata, key="replicate",value = "D_6dpi" ,D_6dpi_1, D_6dpi_2, D_6dpi_3)
D_6dpi<-D_6dpi %>% select(c("replicate", "D_6dpi", "gene_id"))
D_3dpi
newdata<-merge(newdata, D_6dpi, all.x=TRUE, all.y = TRUE)#merge with previous table 

#view new table 
view(newdata)

#################################################################################################################
#ggplots 

# gene PGSC0003DMG400004501
newdata%>%
  filter(gene_id %in% c("PGSC0003DMG400004501"))
  ggplot(aes(gene_id, ) )

data %>% 
  ggplot(aes(x=gene_id=="PGSC0003DMT400063324" , y=M_3dpi_1, M_3dpi_2, M_3dpi_3, R_3dpi_1, R_3dpi_2, R_3dpi_3, 
                                                     R_6dpi_1, R_6dpi_2, R_6dpi_3, D_6dpi_1, D_6dpi_2, D_3dpi_3) )



#M_3dpi gathering
M_3dpi<-gather(mydata, key="replicate",value = "M_3dpi" ,M_3dpi_1, M_3dpi_2, M_3dpi_3)
M_3dpi<-M_3dpi %>% select(c("replicate", "M_3dpi", "gene_id"))
M_3dpi
view(M_3dpi)

#R_3dpi gathering
R_3dpi<-gather(mydata, key="replicate",value = "R_3dpi" ,R_3dpi_1, R_3dpi_2, R_3dpi_3)
R_3dpi<-R_3dpi %>% select(c("replicate", "R_3dpi", "gene_id"))
R_3dpi
view(R_3dpi)
newdata<-merge(M_3dpi, R_3dpi, all.x=TRUE, all.y = TRUE)#merge with previous table 

#D_3dpi gathering
D_3dpi<-gather(mydata, key="replicate",value = "D_3dpi" ,D_3dpi_1, D_3dpi_2, D_3dpi_3)
D_3dpi<-D_3dpi %>% select(c("replicate", "D_3dpi", "gene_id"))
D_3dpi
newdata<-merge(newdata, D_3dpi, all.x=TRUE, all.y = TRUE)#merge with previous table 

#M_6dpi gathering
M_6dpi<-gather(mydata, key="replicate",value = "M_6dpi" ,M_6dpi_1, M_6dpi_2, M_6dpi_3)
M_6dpi<-M_6dpi %>% select(c("replicate", "M_6dpi", "gene_id"))
M_6dpi
newdata<-merge(newdata, M_6dpi, all.x=TRUE, all.y = TRUE)#merge with previous table 

#R_6dpi gathering
R_6dpi<-gather(mydata, key="replicate",value = "R_6dpi" ,R_6dpi_1, R_6dpi_2, R_6dpi_3)
R_6dpi<-R_6dpi %>% select(c("replicate", "R_6dpi", "gene_id"))
R_6dpi
newdata<-merge(newdata, R_6dpi, all.x=TRUE, all.y = TRUE)#merge with previous table 

#D_6dpi gathering
D_6dpi<-gather(mydata, key="replicate",value = "D_6dpi" ,D_6dpi_1, D_6dpi_2, D_6dpi_3)
D_6dpi<-D_6dpi %>% select(c("replicate", "D_6dpi", "gene_id"))
D_3dpi
newdata<-merge(newdata, D_6dpi, all.x=TRUE, all.y = TRUE)#merge with previous table 

#view new table 
view(newdata)





  



