# -------------------------------------------------------------------------------------------------
# ------------------------------------  Author: Daniela Kupper  -----------------------------------
# ------------------------  Title: Soil-Sand Mixture HFA analysis  ---------------------------
# -------------------------------------------------------------------------------------------------

# latest Update: 26.01.2022
# R version. 4.1.1


# 1. Set working directory ------------------------------------------------------------------------

setwd()
# 2. Loading Packages -----------------------------------------------------------------------------

source("GeneralFunctions.R")



# Declare necessary packages
pkgs_cran <- getNecessaryPackages()

# Get the names of all installed packages
inst_pkgs <- installed.packages()[, "Package"]

# Install required packages from CRAN
req_pkgs_cran <- setdiff(pkgs_cran, inst_pkgs)
if (length(req_pkgs_cran) > 0) {
  # Install older version of ffbase, because newest version cannot be installed
  if ('RecordLinkage' %in% req_pkgs_cran) {
    if ('devtools' %in% req_pkgs_cran) {
      install.packages('devtools')
    }
    devtools::install_version("ff", version = "2.2-14.2")
  }
  install.packages(req_pkgs_cran, clean = TRUE)
}

# Load necessary packages
shh <- lapply(c(pkgs_cran), library, character.only = TRUE)

# Purge obsolete variables
rm(inst_pkgs, req_pkgs_cran, shh, pkgs_cran)

# 3. Import dataset -------------------------------------------------------------------------------

# Import dataset

data <-read_excel("sand_soiL_batch1_revised_240122.xlsx") #batch one only 
data$batch<-c("1")#add batch nr
batch2 <- read_excel("soilsand_batch2_revised_240122.xlsx") #batch 2


data$variety <- as.factor(data$variety)
#data$treatment <- as.factor(data$treatment)
data$repetition <- as.factor(data$repetition)
summary(data)

batch2$variety <- as.factor(batch2$variety)
batch2$treatment <- as.factor(batch2$treatment)
batch2$replicate <- as.factor(batch2$replicate)
summary(batch2)

#batches<-merge.data.frame(data, batch2, by="treatment", "pmol.g","batch")

# select potato variety 
PLV <- data %>%
  filter(variety %in% c("PLV"))

B665 <- data %>%
  filter(variety %in% c("B665"))

#select batches 
batch1 <- batch%>%
  filter(batch %in% c("1"))

batch2 <- batch%>%
  filter(batch %in% c("2"))

# Set plotting theme
presentation <- setPlottingTheme(0,0.5)

# 4. Analysis Batch1  -------------------------------------------------------------------------------------

#    4.1 HFA -------------------------------------------------------------------------------------

#        4.1.1 Test for normality -----------------------------------------------------------------

hist(B665$pmol.g)
hist(PLV$pmol.g)
# normality cannot be assumed in my opinion

B665$pmol.g_log <- log(B665$pmol.g)
PLV$pmol.g_log <- log(PLV$pmol.g)
hist(B665$pmol.g_log)
hist(PLV$pmol.g_log)

# Shapiro-Test
# Null hypothesis: The data is normally distributed. If p> 0.05, normality can be assumed.
shapiro.test(B665$pmol.g) 
                #data:   W = 0.89156, p-value = 0.005254 --> not normal --> Kruskal-Wallis test

shapiro.test(PLV$pmol.g)
                # W = 0.93842, p-value = 0.08246 --> normally distributed --> Anova

#QQplot

par(mfrow = c(1, 2))
qqnorm(B665$pmol.g, main = "B665 QQplot")
qqline(B665$pmol.g)
qqnorm(PLV$pmol.g, main = "PLV QQplot")
qqline(PLV$pmol.g)


qqnorm(B665$pmol.g_log, main = "B665 QQplot")
qqline(B665$pmol.g_log)
qqnorm(PLV$pmol.g_log, main = "PLV QQplot")
qqline(PLV$pmol.g_log)



#        4.1.2 PLV analysis -----------------------------------------------------------------------

#PLV analysis --> parametric data 

PLV$treatment <- factor(PLV$treatment, 
                        levels = c("PLV_sa", "PLV_10sl/90sa", "PLV_20sl/80sa",
                                   "PLV_40sl/60sa", "PLV_60sl/40sa", "PLV_sl"),
                        labels = c("100sa", "10sl/90sa", "20sl/80sa",
                                  "40sl/60sa", "60sl/40sa", "100sl")
                        )

par(mfrow = c(1,1))
boxplot(pmol.g ~ treatment, PLV)
boxplot(pmol.g_log ~ treatment, PLV)
model1 <- aov(pmol.g ~ treatment, PLV)
#model1 <- aov(pmol.g_log ~ treatment, PLV)
anova(model1)

TukeyHSD(model1)
plot(TukeyHSD(model1))
abline(v = 0, lty = 2)

#ggplot - boxplot
# analysis of variance
anova <- aov(pmol.g~treatment, PLV)
summary(anova)
# Tukey's test
tukey <- TukeyHSD(anova)
print(tukey)
# compact letter display
cld <- multcompLetters4(anova, tukey, threshold = 0.05)
print(cld)

Tk <- group_by(PLV, treatment) %>%
  summarise(mean = mean(pmol.g), quant = quantile(pmol.g, probs = 0.75)) %>%
  arrange(desc(mean))

# extracting the compact letter display and adding to the Tk table
cld <- as.data.frame.list(cld$treatment)
Tk$cld <- cld$Letters

print(Tk)

# Defining plot constants
textPosV <- 0
textPosH <- 0
text_size <- 8
nudgeY <- 0.01
nudgeX <- 0.1
# Okabe-Ito colorblind palette
cbPalette <- c("#CC79A7", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#D55E00", "#0072B2", "#999999")
#plot to compare with batch2

plotListPLV <- list()
plotListB <- list()
plotListBtwo <- list()

# plotListPLV[[1]] <- createConcentrationPlot(PLV,Tk,0,600)
plotListPLV[[2]] <- createConcentrationPlot(PLV,Tk,0,1600)
#        4.1.3 B665 analysis ----------------------------------------------------------------------

# ggplot line B665
# hist(B665$pmol.g)#histogram 
# boxplot(pmol.g~treatment, B665)

B665$treatment <- factor(B665$treatment, 
                         levels = c("B665_sa", "B665_10sl/90sa", "B665_20sl/80sa", 
                                    "B665_40sl/60sa", "B665_60sl/40sa", "B665_sl"),
                         labels = c("100sa", "10sl/90sa", "20sl/80sa",
                                   "40sl/60sa", "60sl/40sa", "100sl")
                         )

#kuskal-wallis test 
kruskal.test(pmol.g ~ treatment, B665) #Kruskal-Wallis chi-squared = 17.274, df = 5, p-value = 0.004009

#perform Dunn's Test with Bonferroni correction for p-values
DT = dunnTest(pmol.g ~ treatment, data = B665,
            method = "bonferroni")
DT = DT$res
DT

cld = cldList(comparison = DT$Comparison,
            p.value    = DT$P.adj,
            threshold  = 0.05)
cld <- as.data.frame.list(cld)

Pk <- group_by(B665, treatment) %>%
  summarise(mean=mean(pmol.g), quant = quantile(pmol.g, probs = 0.75)) %>%
  arrange(treatment)           # hier die Sortierung nach dem Treatment wie oben bei cld

Pk$cld <- cld$Letter

plotListB[[1]] <- createConcentrationPlot(B665,Pk,0,1600)

grid.arrange(plotListPLV[[2]],plotListB[[1]]) #comparison of PLV and B665 batch1

#  descending plot

#ggplot(B665, aes(x = reorder(treatment, -pmol.g), y = pmol.g)) + 
  #geom_boxplot(aes(fill = treatment), show.legend = FALSE) +
  #geom_point(alpha=0.8, colour = gray(0.2), size = 2) +
  #labs(x = "", y = expression(bold("HFA concentration " ~ (pmol ~ g^{-1} ~ "FRW")))) + 
  #presentation +
  #geom_text(data = Pk, aes(x = treatment, y = quant, label = cld), size = 5, fontface = "bold", vjust = textPosV, hjust = textPosH)


#    4.2 plant size and root weight ---------------------------------------------------------------

# Null hypothesis: The data is normally distributed. If p> 0.05, normality can be assumed.
shapiro.test(B665$plant_size)#data:   W = 0.96028, p-value = 0.3149
shapiro.test(PLV$`plant_size`)# W = 0.95928, p-value = 0.2968

#QQplot
par(mfrow=c(1,2))
qqnorm(B665$plant_size, main = "B665 QQplot")
qqline(B665$plant_size)
qqnorm(PLV$plant_size, main = "PLV QQplot")
qqline(PLV$plant_size)

#Null hypothesis: The data is normally distributed. If p> 0.05, normality can be assumed.
shapiro.test(B665$root_weight)#data:   W = 0.95583, p-value = 0.2415
shapiro.test(PLV$root_weight)# W = 0.94859, p-value = 0.1549


#QQplot
par(mfrow=c(1,2))
qqnorm(B665$root_weight, main = "B665 QQplot")
qqline(B665$root_weight)
qqnorm(PLV$root_weight, main = "PLV QQplot")
qqline(PLV$root_weight)



#       4.2.1 PLV analysis --> parametric data ----------------------------------------------------------

#             4.2.1.1 size ------------------------------------------------------------------------------
# analysis of variance
anova <- aov(plant_size~treatment, PLV)
summary(anova)
# Tukey's test
tukey <- TukeyHSD(anova)
print(tukey)
# compact letter display
cld <- multcompLetters4(anova, tukey, threshold = 0.05)
print(cld)

Tk <- group_by(PLV, treatment) %>%
  summarise(mean=mean(plant_size), quant = quantile(plant_size, probs = 0.75)) %>%
  arrange(desc(mean))

# extracting the compact letter display and adding to the Tk table
cld <- as.data.frame.list(cld$treatment)
Tk$cld <- cld$Letters

print(Tk)

plotListPLV[[3]] <- createPlantSizePlot(PLV,Tk,14,36)

#             4.2.1.2 root weight ----------------------------------------------------------------------------
# analysis of variance
anova <- aov(root_weight~treatment, PLV)
summary(anova)
# Tukey's test
tukey <- TukeyHSD(anova)
print(tukey)
# compact letter display
cld <- multcompLetters4(anova, tukey, threshold = 0.05)
print(cld)

Tk <- group_by(PLV, treatment) %>%
  summarise(mean=mean(root_weight), quant = quantile(root_weight, probs = 0.75)) %>%
  arrange(desc(mean))

# extracting the compact letter display and adding to the Tk table
cld <- as.data.frame.list(cld$treatment)
Tk$cld <- cld$Letters

print(Tk)

plotListPLV[[4]] <- createRootWeightPlot(PLV,Tk,0,16)

grid.arrange(plot3,plot4)

#correlation
cor(PLV$root_weight, PLV$plant_size) #R2: 0.3007215
cor.test(PLV$root_weight, PLV$plant_size) #not significant 
ggplot(PLV, aes(root_weight, plant_size))+
  geom_point(aes(color=treatment, size=pmol.g))#+ #geom_smooth()

cor.test(PLV$pmol.g, PLV$plant_size)#not significant 



#     4.2.2 B665 analysis --> parametric data -----------------------------------------------------
#             4.2.2.1 size ------------------------------------------------------------------------

# analysis of variance
anova <- aov(plant_size~treatment, B665)
summary(anova)
# Tukey's test
tukey <- TukeyHSD(anova)
print(tukey)
# compact letter display
cld <- multcompLetters4(anova, tukey, threshold = 0.05)
print(cld)

Tk <- group_by(B665, treatment) %>%
  summarise(mean=mean(plant_size), quant = quantile(plant_size, probs = 0.75)) %>%
  arrange(desc(mean))

# extracting the compact letter display and adding to the Tk table
cld <- as.data.frame.list(cld$treatment)
Tk$cld <- cld$Letters

plotListB[[2]] <- createPlantSizePlot(B665,Tk,14,36)

#             4.2.2.2 root weight------------------------------------------------------------------------

# analysis of variance
anova <- aov(root_weight~treatment, B665)
summary(anova)
# Tukey's test
tukey <- TukeyHSD(anova)
print(tukey)
# compact letter display
cld <- multcompLetters4(anova, tukey, threshold = 0.05)
print(cld)

Tk <- group_by(B665, treatment) %>%
  summarise(mean=mean(root_weight), quant = quantile(root_weight, probs = 0.75)) %>%
  arrange(desc(mean))

# extracting the compact letter display and adding to the Tk table
cld <- as.data.frame.list(cld$treatment)
Tk$cld <- cld$Letters

print(Tk)

plotListB[[3]] <- createRootWeightPlot(B665,Tk,0,16)

grid.arrange(plot5,plot6, ncol=1, nrow=2)
grid.arrange(plot4,plot6, ncol=2, nrow=1)

grid.arrange(plot1.2,plot2,plot3,plot5,plot4,plot6)
grid.arrange(plot1,plot2,plot3,plot5,plot4,plot6, ncol=2, nrow=3, with)

#correlation
cor(B665$root_weight, B665$plant_size) #0.8134938
cor.test(B665$root_weight, B665$plant_size)
#linear equation result: y = 1.96*x + 20.33
summary(lm(plant_size ~ root_weight, data=B665))

plotListB[[4]] <- ggplot(B665, aes(root_weight, plant_size))+
  geom_point(aes(color=treatment, size=pmol.g)) +
  scale_size(name = "pmol/g")+
  labs(x=expression(bold("plant size " ~ (cm))),y=expression(bold("root weight " ~ (g))))+
  presentation+
  geom_smooth(method='lm')+
  geom_text(aes(x=4.2, y=36, label="y = 1.96x + 20.33"), size = text_size, fontface = "bold", hjust=0)+
  geom_text(aes(x=4.2, y=35, label="r = 0.81, p < 0.01"), size = text_size, hjust=0)
  
#    4.3 descriptive statistics ----------------------------------------------------------------------------
#       4.3.1 PLV------------------------------------------------------------------------------------

summary(PLV$pmol.g)
describeBy(PLV$pmol.g, group=PLV$treatment)

summary(PLV$root_weight)
describeBy(PLV$root_weight, group=PLV$treatment)

summary(PLV$plant_size)
describeBy(PLV$plant_size, group=PLV$treatment)


#       4.3.1 B665---------------------------------------------------------------------------------

summary(B665$pmol.g)
describeBy(B665$pmol.g, group=B665$treatment)

summary(B665$root_weight)
describeBy(B665$root_weight, group=B665$treatment)

summary(B665$plant_size)
describeBy(B665$plant_size, group=B665$treatment)


#  5. Analysis Batch2 -----------------------------------------------------------------------------

#    5.1 HFA -------------------------------------------------------------------------------------

#        5.1.1 Test for normality -----------------------------------------------------------------

#bring batch2 in a order
batch2$treatment <- factor(batch2$treatment,
                           levels = c("PLV_sa", "PLV_10sl/90sa", "PLV_20sl/80sa",
                                      "PLV_40sl/60sa", "PLV_60sl/40sa", "PLV_sl"),
                           labels = c("100sa", "10sl/90sa", "20sl/80sa",
                                      "40sl/60sa", "60sl/40sa", "100sl")
                           )
hist(batch2$pmol.g)
# normality cannot be assumed in my opinion

batch2$pmol.g_log <- log(batch2$pmol.g)
hist(batch2$pmol.g_log)


# Shapiro-Test
# Null hypothesis: The data is normally distributed. If p> 0.05, normality can be assumed.
shapiro.test(batch2$pmol.g) 
#data: W = 0.90116, p-value = 0.0005994 --> non-parametric

shapiro.test(batch2$pmol.g_log) #data:  W = 0.96413, p-value = 0.14


#QQplot

qqnorm(batch2$pmol.g, main = "batch2 QQplot")
qqline(batch2$pmol.g)

qqnorm(batch2$pmol.g_log, main = "batch2 transformed QQplot")
qqline(batch2$pmol.g_log)


#kuskal-wallis test 
kruskal.test(pmol.g ~ treatment, batch2) #Kruskal-Wallis chi-squared = 12.41, df = 5, p-value = 0.02958
dunn.test(batch2$pmol.g, batch2$treatment,method = "bonferroni" )

#perform Dunn's Test with Bonferroni correction for p-values
DT=dunnTest(pmol.g ~ treatment, data = batch2, na.action(na.rm=TRUE),
            method = "bonferroni")
DT = DT$res
DT


cld=cldList(comparison = DT$Comparison,
            p.value    = DT$P.adj,
            threshold  = 0.05)
cld <- as.data.frame.list(cld)
cld

Pk <- group_by(batch2, treatment) %>%
  summarise(mean = mean(pmol.g, na.rm = TRUE), quant = quantile(pmol.g, probs = 0.75, na.rm = TRUE)) %>%
  arrange(treatment)

Pk$cld <- cld$Letter

#vorher plot7
plotListBtwo[[1]] <- createConcentrationNoTextPlot(batch2,0,120)

#    5.2 root weight -------------------------------------------------------------------------------------

#        5.1.1 Test for normality -----------------------------------------------------------------

hist(batch2$root_weight)
# normality can be assumed


# Shapiro-Test
# Null hypothesis: The data is normally distributed. If p> 0.05, normality can be assumed.
shapiro.test(batch2$root_weight) #0.9849, p-value = 0.6649 --> normal 


#QQplot

qqnorm(batch2$root_weight, main = "batch2 root weight QQplot")
qqline(batch2$root_weight)


# analysis of variance
anova <- aov(root_weight~treatment, batch2)
summary(anova)
# Tukey's test
tukey <- TukeyHSD(anova)
print(tukey)
# compact letter display
cld <- multcompLetters4(anova, tukey, threshold = 0.05)
print(cld)

Tk <- group_by(batch2, treatment) %>%
  summarise(mean=mean(root_weight), quant = quantile(root_weight, probs = 0.75)) %>%
  arrange(desc(mean))

# extracting the compact letter display and adding to the Tk table
cld <- as.data.frame.list(cld$treatment)
Tk$cld <- cld$Letters

print(Tk)

plotListBtwo[[2]] <- createRootWeightPlot(batch2,Tk,0,6)

grid.arrange(plotListBtwo[[1]], plotListBtwo[[2]], nrow=1, ncol=2)

#    5.3 descriptive statistics ----------------------------------------------------------------------------
#       5.3.1 PLV------------------------------------------------------------------------------------

summary(batch2$pmol.g)
describeBy(batch2$pmol.g, group=batch2$treatment)

summary(batch2$root_weight)
describeBy(batch2$root_weight, group=batch2$treatment)



#  6. Analysis Batch 1 and 2 ---------------------------------------------------------------------------------------

#    6.1 HFAA -------------------------------------------------------------------------------------

PLVbatch<-batch%>%
  filter(variety %in% c("PLV"))

plot9<- ggplot(PLVbatch, aes(treatment, pmol.g), color=blue) + 
  geom_boxplot(aes(fill = treatment), show.legend = FALSE) +
  geom_point(alpha=0.8, colour= gray(0.2), size=2)+
  facet_grid(~batch)+
  labs(x="",y=expression(bold("HFA concentration " ~ (pmol ~ g^{-1} ~ "FRW"))))+ 
  presentation +
  scale_fill_manual(values=cbPalette)+
  geom_text(data = Pk, aes(x = treatment, y = quant, label = cld), size = 5, fontface="bold", vjust = textPosV, hjust = textPosH)

plot9

plot10<- ggplot(PLVbatch, aes(treatment,root_weight), color=blue) + 
  geom_boxplot(aes(fill = treatment), show.legend = FALSE) +
  geom_point(alpha=0.8, colour= gray(0.2), size=2)+
  facet_grid(~batch)+
  labs(x="",y=expression(bold("weight " ~ ("g"))))+ 
  presentation +
  scale_fill_manual(values=cbPalette)+
geom_text(data = Pk, aes(x = treatment, y = quant, label = cld), size = 5, fontface="bold", vjust = textPosV, hjust = textPosH)

plot10
grid.arrange(plot1, plot7)

#           ---------------------------------------------------
#Save all plots
saveThisPlotListAsPng(plotListPLV,"PLV")
saveThisPlotListAsPng(plotListB,"B665")
saveThisPlotListAsPng(plotListBtwo,"batch2")
graphics.off()


