# -------------------------------------------------------------------------------------------------
# ------------------------------------  Author: Daniela Kupper  -----------------------------------
# ------------------------  Title: QTL silencing experiment in tomato (1)  ------------------------
# -------------------------------------------------------------------------------------------------

# latest Update: 14.02.2022
# R version. 4.1.1


# 1. Set working directory ------------------------------------------------------------------------

 
setwd()

# 2. Loading Packages -----------------------------------------------------------------------------

source("GeneralFunctions.R")

# Mit diesem Code werden die noch nicht installierten Packages heruntergeladen und sonst werden sie
# in die Library geladen.

# Declare necessary packages
pkgs_cran <- getNecessaryPackages()

# Get the names of all installed packages
inst_pkgs <- installed.packages()[, "Package"]

# Install required packages from CRAN
req_pkgs_cran <- setdiff(pkgs_cran, inst_pkgs)
if (length(req_pkgs_cran) > 0) {
  # Install older version of ffbase, because newest version cannot be installed
  if ('RecordLinkage' %in% req_pkgs_cran) {
    if ('devtools' %in% req_pkgs_cran) {
      install.packages('devtools')
    }
    devtools::install_version("ff", version = "2.2-14.2")
  }
  install.packages(req_pkgs_cran, clean = TRUE)
}

# Load necessary packages
shh <- lapply(c(pkgs_cran), library, character.only = TRUE)

# Purge obsolete variables
rm(inst_pkgs, req_pkgs_cran, shh, pkgs_cran)


#  3. Import data sets----------------------------------------------------------------------------------------------------

#data with PDS batch1
mydataPDS <- read_excel("HFA_tomato1_analysis.xlsx")
#View(mydataPDS) 
mydataPDS$treatment <- as.factor(mydataPDS$treatment)
mydataPDS$gene <- as.factor(mydataPDS$gene)
mydataPDS$gene_abr <- as.factor(mydataPDS$gene_abr)
mydataPDS$replicate <- as.factor(mydataPDS$replicate)
summary(mydataPDS)

#data without PDS batch
mydata   <- read_excel("HFA_tomato_analysis_withoutPDS.xlsx")
#view(mydata)
mydata$treatment <- as.factor(mydata$treatment)
mydata$gene <- as.factor(mydata$gene)
mydata$gene_abr <- as.factor(mydata$gene_abr)
mydata$replicate <- as.factor(mydata$replicate)
summary(mydata)

#data batch2
#batch2<- read_excel("HFA_tomato_batch2_analysis.xlsx") ####wrong calculations
batch2<- read_excel("HFA_batch2_tomato_revised-240122.xlsx")
batch2$gene <- as.factor(batch2$gene)
batch2$gene_abr <- as.factor(batch2$gene_abr)
batch2$replicate <- as.factor(batch2$replicate)
summary(batch2)

#data qpcr
#batch1
qpcr<- read_excel("qPCR_QTL9_GUS_07022022.xlsx")
qpcr$gene_abrz <- as.factor(qpcr$gene_abr)
qpcr$Sample_Biological_Group <- as.factor(qpcr$Sample_Biological_Group)
qpcr$norm_expression <- as.numeric(qpcr$Expression)
summary(qpcr)

#batch2
qpcr2 <- read_excel("qPCRBatch2QTL9.xlsx")
qpcr2$gene_abrz <- as.factor(qpcr2$gene_abr)
qpcr2$Sample_Biological_Group <- as.factor(qpcr2$Sample_Biological_Group)
qpcr2$norm_expression <- as.numeric(qpcr2$Expression)
summary(qpcr2)

# Set plotting theme
presentation <- setPlottingTheme(30,1)

#set order of genes (3β-HSD is probemati!9
setDT(mydata)
mydata[mydata$treatment == "QTL9", ]$gene_abr <- "3BHSD"
mydata[mydata$treatment == "QTL1", ]$gene_abr <- "SSR2"
mydata[mydata$treatment == "QTL6", ]$gene_abr <- "AllylETOHdh"

setDT(mydataPDS)
mydataPDS[mydataPDS$treatment == "QTL9", ]$gene_abr <- "3BHSD"
mydataPDS[mydataPDS$treatment == "QTL1", ]$gene_abr <- "SSR2"
mydataPDS[mydataPDS$treatment == "QTL6", ]$gene_abr <- "AllylETOHdh"

setDT(batch2)
batch2[batch2$treatment == "QTL9", ]$gene_abr <- "3BHSD"
batch2[batch2$treatment == "untreated (Ut)", ]$gene_abr <- "untreated"
batch2[batch2$treatment == "QTL1", ]$gene_abr <- "SSR2"
batch2[batch2$treatment == "QTL6", ]$gene_abr <- "AllylETOHdh"


#select genes for specific plots when comparing batches, prepare new table
data_b1<-mydata %>%
  filter(gene_abr %in% c("SSR2", "ETOHdh", "AllylETOHdh", "3BHSD", 
                         "St22ds", "Control (GUS)"))
data_b2<-batch2 %>%
  filter(gene_abr%in% c("SSR2", "ETOHdh", "AllylETOHdh", "3BHSD", 
                        "St22ds", "Control (GUS)"))

qpcrData<-qpcr %>%
  filter(gene_abr%in% c("3BHSD", "Control (GUS)"))

qpcrDataTwo<-qpcr %>%
  filter(gene_abr%in% c("3BHSD", "Control (GUS)")) %>%
  filter(!Sample_Biological_Group %in% ("QTL9-4_QTL9"))

qpcrDatab2<-qpcr2 %>%
  filter(gene_abr%in% c("3BHSD", "Control (GUS)"))

# to bing a order in the plots
mydata$gene_abr <- factor(mydata$gene_abr, levels = c("SSR2", "ETOHdh", "AllylETOHdh", "3BHSD", 
                                             "St22ds", "LBD16", "RHL1", "Control (GUS)","Mock", "untreated"))

mydataPDS$gene_abr<- factor(mydataPDS$gene_abr, levels = c("SSR2", "ETOHdh", "AllylETOHdh", "3BHSD", 
                                                     "St22ds", "LBD16", "RHL1", "PDS", "Control (GUS)",
                                                     "Mock", "untreated"))

batch2$gene_abr <- factor(batch2$gene_abr, levels = c("SSR2", "ETOHdh", "AllylETOHdh", "3BHSD", 
                                                      "St22ds", "PDS", "Control (GUS)","untreated"))

data_b1$gene_abr<- factor(data_b1$gene_abr, levels = c("SSR2", "ETOHdh", "AllylETOHdh", "3BHSD", 
                                                    "St22ds", "Control (GUS)"))

data_b2$gene_abr<- factor(data_b2$gene_abr, levels = c("SSR2", "ETOHdh", "AllylETOHdh", "3BHSD", 
                                                    "St22ds", "Control (GUS)"))


#  4. Analysis --------------------------------------------------------------------------------------

#     4.1 HFA analysis -----------------------------------------------------------------------------

#     4.1.1 test for normality ---------------------------------------------------------------------

hist(mydata$pmol.g) 

#Null hypothesis: The data is normally distributed. If p> 0.05, normality can be assumed.
shapiro.test(mydata$`pmol.g`)#data:   W = 0.97215, p-value = 0.3457


#QQplot
par(mfrow=c(1,2))
qqnorm(mydata$`pmol.g`, main = "tomato1 HFA QQplot")
qqline(mydata$`pmol.g`)

### descriptive statistics #####
summary(mydata$pmol.g)
describeBy(mydata$pmol.g, group = mydata$gene_abr)

summary(data_b1$pmol.g)


#     4.1.2 analysis HFA--> parametric data ---------------------------------------------------------------------

par(mfrow=c(1,1))
boxplot(pmol.g~gene_abr, mydata)
model1 <- aov(pmol.g~gene_abr, mydata)
anova(model1)
TukeyHSD(model1)
plot(TukeyHSD(model1))
abline(v=0, lty=2)

#ggplot - boxplot
# analysis of variance
anova <- aov(pmol.g~gene_abr, mydata)
summary(anova)
# Tukey's test
tukey <- TukeyHSD(anova)
print(tukey)
# compact letter display
cld <- multcompLetters4(anova, tukey, threshold = 0.05)
print(cld)

Tk <- group_by(mydata, gene_abr) %>%
  summarise(mean=mean(pmol.g), quant = quantile(pmol.g, probs = 0.75)) %>%
  arrange(desc(mean))

# extracting the compact letter display and adding to the Tk table
cld <- as.data.frame.list(cld$gene_abr)
Tk$cld <- cld$Letters

print(Tk)

# Defining plot constants
textPosV <- 0
textPosH <- 0
text_size <- 8
nudgeY <- 0.01
nudgeX <- 0.1
# Defining colors
cbPalette <- c("#FF0000", "#FF8000", "#FFFF00", "#80FF00", "#14761A", "#D0FFFF", "#46A1FB", "#0000FF", "#8000FF", "#FF00FF", "#FCDEEB", "#FF0080")

plotListTomatoOne <- list()
plotListTomatoTwo <- list()
plotListQpcrBatchOne <- list()
plotListQpcrBatchTwo <- list()

plotListTomatoOne[[1]] <- createTomatoConcentrationPlot(mydata,Tk,20,300)

#plot with specific genes
par(mfrow=c(1,1))
boxplot(pmol.g~gene_abr, data_b1)
model1 <- aov(pmol.g~gene_abr, data_b1)
anova(model1)
TukeyHSD(model1)
plot(TukeyHSD(model1))
abline(v=0, lty=2)

#ggplot - boxplot
# analysis of variance
anova <- aov(pmol.g~gene_abr, data_b1)
summary(anova)
# Tukey's test
tukey <- TukeyHSD(anova)
print(tukey)
# compact letter display
cld <- multcompLetters4(anova, tukey, threshold = 0.05)
print(cld)

hk <- group_by(data_b1, gene_abr) %>%
  summarise(mean=mean(pmol.g), quant = quantile(pmol.g, probs = 0.75)) %>%
  arrange(desc(mean))

# extracting the compact letter display and adding to the Tk table
cld <- as.data.frame.list(cld$gene_abr)
hk$cld <- cld$Letters

print(hk)

# vorher plot1.1
plotListTomatoOne[[2]] <- createTomatoConcentrationPlot(data_b1,hk,20,300)

#     4.2 plant size and root weight --------------------------------------------------------------          

#        4.2.1 test for normality ----------------------------------------------------------------

hist(mydata$weight) 
hist(mydataPDS$size_17.09.21)
hist(mydataPDS$size_27.9.21)
hist(data_b1$weight)
hist(data_b1$size_27.9.21)

#Null hypothesis: The data is normally distributed. If p> 0.05, normality can be assumed.
shapiro.test(mydata$weight)#data:   W = 0.94434, p-value = 0.03112 --> not normal 

shapiro.test(mydataPDS$size_17.09.21) #W = 0.97285, p-value = 0.2009 --> normal
shapiro.test(mydata$size_17.09.21)#W = 0.98455, p-value = 0.8029 --> normal

shapiro.test(mydataPDS$size_27.9.21)# W = 0.91241, p-value = 0.0003846 --> not normal
shapiro.test(mydata$size_27.9.21) #W = 0.90335, p-value = 0.001206 --> not normal

shapiro.test(data_b1$weight) #W = 0.94842, p-value = 0.1533
shapiro.test(data_b1$size_27.9.21) #W = 0.8761, p-value = 0.002297


#QQplot
par(mfrow=c(1,2))
qqnorm(mydata$weight, main = "tomato1 HFA QQplot")
qqline(mydata$weight)

par(mfrow=c(1,2))
qqnorm(mydataPDS$size_17.09.21, main = "tomato1 size 17.09.21")
qqline(mydataPDS$size_17.09.21)

par(mfrow=c(1,2))
qqnorm(mydataPDS$size_27.9.21, main = "tomato1 size 27.09.21")
qqline(mydataPDS$size_27.9.21)
qqnorm(mydata$size_27.9.21, main = "tomato1 size 27.09.21 without PDS and untreated")
qqline(mydata$size_27.9.21)

par(mfrow=c(1,2))
qqnorm(data_b1$weight, main = "weight tomato1 HFA QQplot specific genes")
qqline(data_b1$weight)
qqnorm(data_b1$size_27.9.21, main = "plant size tomato1 HFA QQplot specific genes")
qqline(data_b1$size_27.9.21)


#        4.2.2 descriptive statistics weight and size------------------------------------------------

summary(mydata$weight)
describeBy(mydata$weight, group = mydata$gene_abr)

summary(data_b1$weight)

summary(mydataPDS$size_17.09.21)
describeBy(mydataPDS$size_17.09.21, group = mydataPDS$gene_abr)

summary(data_b1$size_17.09.21)

summary(mydataPDS$size_27.9.21)
describeBy(mydataPDS$size_27.9.21, group = mydataPDS$gene_abr)

summary(data_b1$size_27.9.21)


#        4.2.3 analysis weight --------------------------------------------------------

#kuskal-wallis test 
kruskal.test(weight~gene_abr, mydata) #Kruskal-Wallis chi-squared = 21.135, df = 8, p-value = 0.006799

#perform Dunn's Test with Bonferroni correction for p-values
DT=dunnTest(weight~gene_abr, data=mydata,
            method="bonferroni")
DT = DT$res
DT


cld=cldList(comparison = DT$Comparison,
            p.value    = DT$P.adj,
            threshold  = 0.05)
cld <- as.data.frame.list(cld)
names(cld)<-c("gene_abr","Letter", "MonoLetter")
cld
setDT(cld)
cld[cld$gene_abr == "Control(GUS)", ]$gene_abr <- "Control (GUS)"
Pk <- group_by(mydata, gene_abr) %>%
  summarise(mean=mean(weight), quant = quantile(weight, probs = 0.75)) %>%
  arrange(gene_abr)
Pk<-inner_join(cld, Pk, by="gene_abr")


# vorher plot2
plotListTomatoOne[[3]] <-  createTomatoRootWeightPlot(mydata,Pk,0,10)

#plot specific genes 
model1 <- aov(weight~gene_abr, data_b1)
anova(model1)
TukeyHSD(model1)
plot(TukeyHSD(model1))
abline(v=0, lty=2)

# vorher plot2.1
plotListTomatoOne[[4]] <- createTomatoRootWeightNoTextPlot(data_b1,Tk,0,7)


  #         4.2.4 analysis size 17.9.21 --------------------------------------------------
  
  #ggplot - boxplot
  # analysis of variance
  anova <- aov(size_17.09.21~gene_abr, mydataPDS)
  summary(anova)
  # Tukey's test
  tukey <- TukeyHSD(anova)
  print(tukey)
  # compact letter display
  cld <- multcompLetters4(anova, tukey, threshold = 0.05)
  print(cld)
  
  Vk <- group_by(mydataPDS, gene_abr) %>%
    summarise(mean=mean(size_17.09.21), quant = quantile(size_17.09.21, probs = 0.75)) %>%
    arrange(desc(mean))
  
  # extracting the compact letter display and adding to the Tk table
  cld <- as.data.frame.list(cld$gene_abr)
  Vk$cld <- cld$Letters
  
  print(Vk)

    # vorher plot3: plant size 26 dpi, 17.09.21
  plotListTomatoOne[[5]] <- createTomatoPlantSizePlot(mydataPDS,Vk, 7, 18)
  
#         4.2.5 analysis size 27.9.21---------------------------------------------------
  
#kuskal-wallis test 
  kruskal.test(size_27.9.21~gene_abr, mydataPDS) #Kruskal-Wallis chi-squared = 22.539, df = 10, p-value = 0.01258

#perform Dunn's Test with Bonferroni correction for p-values
  DT=dunnTest(size_27.9.21~gene_abr, data=mydataPDS,
              method="bonferroni")
  DT = DT$res
  DT
  
  
  cld=cldList(comparison = DT$Comparison,
              p.value    = DT$P.adj,
              threshold  = 0.05)
  cld <- as.data.frame.list(cld)
  names(cld)<-c("gene_abr","Letter", "MonoLetter")
  cld
  setDT(cld)
  cld[cld$gene_abr == "Control(GUS)", ]$gene_abr <- "Control (GUS)"
  Lk <- group_by(mydataPDS, gene_abr) %>%
    summarise(mean=mean(size_27.9.21), quant = quantile(size_27.9.21, probs = 0.75)) %>%
    arrange(gene_abr)
  Lk<-inner_join(cld, Lk, by="gene_abr")
  
# vorher plot4, 27.9.21 - 42 dpi
plotListTomatoOne[[6]] <- createTomatoPlantSizePlotFortyTwo(mydataPDS,Lk,15,50)

#plot specific genes

#kuskal-wallis test 
kruskal.test(size_27.9.21~gene_abr, data_b1) #Kruskal-Wallis chi-squared = 2.7049, df = 5, p-value = 0.7454

# vorher plot4.1 27.9.21 - 42 dpi
plotListTomatoOne[[7]] <- createTomatoPlantSizePlotFortyTwoNoText(data_b1,10,40,"plant size 42 dpi ")


#kuskal-wallis test without PDS
kruskal.test(size_27.9.21~gene_abr, mydata) #Kruskal-Wallis chi-squared = 7.4332, df = 8, p-value = 0.4907

#perform Dunn's Test with Bonferroni correction for p-values
DT=dunnTest(size_27.9.21~gene_abr, data=mydata,
            method="bonferroni")
DT = DT$res
DT


cld=cldList(comparison = DT$Comparison,
            p.value    = DT$P.adj,
            threshold  = 0.05)
cld <- as.data.frame.list(cld)
names(cld)<-c("gene_abr","Letter", "MonoLetter")
cld
setDT(cld)
cld[cld$gene_abr == "Control(GUS)", ]$gene_abr <- "Control (GUS)"
Lk <- group_by(mydataPDS, gene_abr) %>%
  summarise(mean=mean(size_27.9.21), quant = quantile(size_27.9.21, probs = 0.75)) %>%
  arrange(gene_abr)
Lk<-inner_join(cld, Lk, by="gene_abr")

# vorher plot5 27.9.21  26 dpi
plotListTomatoOne[[8]] <- createTomatoPlantSizePlotFortyTwoNoText(mydata,15,50,"plant size 26 dpi ")
  
#           4.2.6 correlation weight vs size---------------------------------------------------
  
cor(mydata$size_27.9.21, mydata$weight)#0.2938217
cor.test(mydata$size_27.9.21, mydata$weight)
  ggplot(mydata, aes(weight, size_27.9.21))+
    geom_point(aes(color=gene_abr))#+
   
cor(mydata$size_27.9.21, mydata$size_17.09.21) #] 0.5350982
cor.test(mydata$size_27.9.21, mydata$size_17.09.21)
    ggplot(mydata, aes(size_17.09.21, size_27.9.21))+
      geom_point(aes(color=gene_abr))#+
    
cor.test(data_b1$size_27.9.21, data_b1$weight)
cor.test(data_b2$plant_size, data_b2$root_weight) #p=0.02163, #cor -0.2960592 
ggplot(data_b2, aes(plant_size, root_weight))+
  geom_point(aes(color=gene_abr, size=pmol.g))+
  geom_smooth(method = "lm")

    
    
#  5. Analysis batch 2 --------------------------------------------------------------------------------------
    
#      5.1 HFA analysis -----------------------------------------------------------------------------
    
    #     5.1.1 test for normality ---------------------------------------------------------------------
    
hist(batch2$pmol.g) 
    
#Null hypothesis: The data is normally distributed. If p> 0.05, normality can be assumed.
shapiro.test(batch2$`pmol.g`)#data:  W = 0.96794, p-value = 0.042
    
    
#QQplot
par(mfrow=c(1,2))
qqnorm(batch2$`pmol.g`, main = "tomato2 HFA QQplot")
qqline(batch2$`pmol.g`)
    
### descriptive statistics #####
summary(batch2$pmol.g)
describeBy(batch2$pmol.g, group = batch2$gene_abr)

summary(data_b2$pmol.g)

#     5.1.2 analysis SolA batch2 --> non-parametric data ---------------------------------------------------------------------

#kuskal-wallis test 
kruskal.test(pmol.g~gene_abr, batch2) #Kruskal-Wallis Kruskal-Wallis chi-squared = 16.627, df = 7, p-value = 0.01996

#perform Dunn's Test with Bonferroni correction for p-values
DT=dunnTest(pmol.g~gene_abr, data= batch2[complete.cases(batch2$pmol.g),],
            method="bonferroni")
DT = DT$res
DT

cld=cldList(comparison = DT$Comparison,
            p.value    = DT$P.adj,
            threshold  = 0.05)
cld <- as.data.frame.list(cld)
names(cld)<-c("gene_abr","Letter", "MonoLetter")
cld
setDT(cld)
cld[cld$gene_abr == "Control(GUS)", ]$gene_abr <- "Control (GUS)"
Qk <- group_by(batch2, gene_abr) %>%
  ## ****** na.rm=TRUE inserted ********
  summarise(mean=mean(pmol.g), quant = quantile(pmol.g, probs = 0.75, na.rm=TRUE)) %>%
  arrange(gene_abr)
Qk<-inner_join(cld, Qk, by="gene_abr")
Qk
cld

#vorher plot6
plotListTomatoTwo[[1]] <- createTomatoConcentrationLetterPlot(batch2,Qk,0,50)

#plot with specific genes
hist(data_b2$pmol.g) 
shapiro.test(data_b2$pmol.g) #W = 0.9654, p-value = 0.0867 --> parametric 

qqnorm(data_b2$`pmol.g`, main = "tomato2 HFA QQplot specific genes")
qqline(data_b2$`pmol.g`)

par(mfrow=c(1,1))
boxplot(pmol.g~gene_abr,data_b2)
model1 <- aov(pmol.g~gene_abr, data_b2)
anova(model1)
TukeyHSD(model1)
plot(TukeyHSD(model1))
abline(v=0, lty=2)

#ggplot - boxplot
# analysis of variance
anova <- aov(pmol.g~gene_abr, data_b2)
summary(anova)
# Tukey's test
tukey <- TukeyHSD(anova)
print(tukey)
# compact letter display
cld <- multcompLetters4(anova, tukey, threshold = 0.05)
print(cld)

Tk <- group_by(data_b2, gene_abr) %>%
  summarise(mean=mean(pmol.g), quant = quantile(pmol.g, probs = 0.75, na.rm=TRUE)) %>%
  arrange(desc(mean))
Tk
cld
# extracting the compact letter display and adding to the Tk table
cld <- as.data.frame.list(cld$gene_abr)
# Inserted manually according cld due to error in sequence when joining
newColumn = list("a","ab","ab","b","ab","ab")
Tk$cld <- newColumn

print(Tk)


#**************

# vorher plot6.2
plotListTomatoTwo[[2]] <- createTomatoConcentrationPlot(data_b2,Tk,20,300)

# vorher Plot6.3
plotListTomatoTwo[[3]] <- createTomatoConcentrationPlot(data_b2,Tk,0,60)

#      5.2 analysis root weight and size --------------------------------------

#         5.2.1 test for normality -----------------------------------------------------------------

hist(batch2$root_weight) 
hist(batch2$plant_size)
hist(data_b2$root_weight)
hist(data_b2$plant_size)

#Null hypothesis: The data is normally distributed. If p> 0.05, normality can be assumed.
shapiro.test(batch2$root_weight)#data:   W = 0.9739, p-value = 0.1007
shapiro.test(batch2$plant_size) # data: W = 0.98442, p-value = 0.4503
shapiro.test(data_b2$root_weight) #W = 0.94993, p-value = 0.01545 --> not normal
shapiro.test(data_b2$plant_size)  #W = 0.97431, p-value = 0.2359


#QQplot
par(mfrow=c(1,2))
qqnorm(batch2$root_weight, main = "tomato 2 root weight QQplot")
qqline(batch2$root_weight)

qqnorm(batch2$plant_size, main = "tomato 2 plant size QQplot")
qqline(batch2$plant_size)
       

### descriptive statistics #####
summary(batch2$root_weight)
describeBy(batch2$root_weight, group = batch2$gene_abr)

summary(data_b2$root_weight)

summary(batch2$plant_size)
describeBy(batch2$plant_size, group = batch2$gene_abr)

summary(data_b2$plant_size)

#         5.2.2 root weight analysis -------------------------------------------------------------    

# analysis of variance
anova <- aov(root_weight~gene_abr, batch2)
summary(anova)
# Tukey's test
tukey <- TukeyHSD(anova)
print(tukey)
# compact letter display
cld <- multcompLetters4(anova, tukey, threshold = 0.05)
print(cld)

Wk <- group_by(batch2, gene_abr) %>%
  summarise(mean=mean(root_weight), quant = quantile(root_weight, probs = 0.75)) %>%
  arrange(desc(mean))

# extracting the compact letter display and adding to the Tk table
cld <- as.data.frame.list(cld$gene_abr)
Wk$cld <- cld$Letters

print(Wk)

#vorher plot7
plotListTomatoTwo[[4]] <- createTomatoRoot_rootWeightPlot(batch2,Wk,0,3.5)

#plot for specifg genes

#kuskal-wallis test 
kruskal.test(root_weight~gene_abr, data_b2) #Kruskal-Wallis chi-squared = 4.8222, df = 5, p-value = 0.438

#vorher plot7.1
plotListTomatoTwo[[5]] <-createTomatoRoot_rootWeightNoTextPlot(data_b2,0,7)

#         5.2.3 plant size batch 2 -------------------------------------------------------------    

# analysis of variance
anova <- aov(plant_size~gene_abr, batch2)
summary(anova)
# Tukey's test
tukey <- TukeyHSD(anova)
print(tukey)
# compact letter display
cld <- multcompLetters4(anova, tukey, threshold = 0.05)
print(cld)

Dk <- group_by(batch2, gene_abr) %>%
  summarise(mean=mean(plant_size), quant = quantile(plant_size, probs = 0.75, na.rm=TRUE)) %>%
  arrange(desc(mean))

# extracting the compact letter display and adding to the Tk table
cld <- as.data.frame.list(cld$gene_abr)
Dk$cld <- cld$Letters

print(Dk)

#vorher plot8
plotListTomatoTwo[[6]] <- createTomatoPlantSizeBatchTwoNoText(batch2,10,25,"plant size 42 dpi")

#plot specific genes
anova <- aov(plant_size~gene_abr, data_b2)
summary(anova)

#vorher plot8.1
plotListTomatoTwo[[7]] <- createTomatoPlantSizeBatchTwoNoText(data_b2,10,40,"plant size 42 dpi")


#           5.2.4 correlation weight vs size---------------------------------------------------

cor.test(batch2$plant_size, batch2$root_weight) #p-value = 0.005622, sample estimates:cor -0.3088112 
ggplot(batch2, aes(root_weight, plant_size))+
  geom_point(aes(color=gene_abr))+
  geom_smooth(method = "lm")

cor.test(data_b2$plant_size, data_b2$root_weight) # p-value = 0.02163,  cor -0.2960592 
ggplot(data_b2, aes(root_weight, plant_size))+
  geom_point(aes(color=gene_abr))+
  geom_smooth(method = "lm")

#  6. qPCR data 3BHSD ---------------------------------------------------

#      6.1 qPCR plots both batches----------------------------------------
#batch1
my_comparisons <- list( c("3BHSD", "Control (GUS)"))
cbPaletteC <- c("#80FF00", "#D0FFFF")
plotListQpcrBatchOne[[1]] <- ggplot(qpcrData, aes(gene_abr, norm_expression)) + 
  geom_boxplot(aes(fill = gene_abr), show.legend = FALSE) +
  geom_point(alpha=0.8, colour= gray(0.2), size=2)+
  labs(x="",y=expression(bold("Relative Normalised Expression ")))+ 
  presentation+
  ggpubr::stat_compare_means(comparisons = my_comparisons, method="t.test", size=6.5) +
  coord_cartesian(ylim=c(0,2), expand = TRUE)+
  scale_fill_manual(values=cbPaletteC)

plotListQpcrBatchOne[[2]] <- ggplot(qpcrDataTwo, aes(gene_abr, norm_expression)) + 
  geom_boxplot(aes(fill = gene_abr), show.legend = FALSE) +
  geom_point(alpha=0.8, colour= gray(0.2), size=2)+
  labs(x="",y=expression(bold("Relative Normalized Expression ")))+ 
  presentation+
  ggpubr::stat_compare_means(comparisons = my_comparisons, method="t.test", size=6.5) +
  coord_cartesian(ylim=c(0,2), expand = TRUE)+
  scale_fill_manual(values=cbPaletteC)

#batch2
plotListQpcrBatchTwo[[1]] <- ggplot(qpcrDatab2, aes(gene_abr, norm_expression)) + 
  geom_boxplot(aes(fill = gene_abr), show.legend = FALSE) +
  geom_point(alpha=0.8, colour= gray(0.2), size=2)+
  labs(x="",y=expression(bold("Relative Normalized Expression ")))+ 
  presentation+
  ggpubr::stat_compare_means(comparisons = my_comparisons, method="t.test", size=6.5) +
  coord_cartesian(ylim=c(0,2), expand = TRUE)+
  scale_fill_manual(values=cbPaletteC)

summary(qpcrDatab2)
describeBy(qpcrDatab2$norm_expression, group = qpcrDatab2$gene_abr)

#     6.2 HFA level after correction for silenced -----------------------------

#plot SHFA after silencing 
qpcrHFA<-batch2 %>%
  filter(gene_abr%in% c("3BHSD", "Control (GUS)")) %>%
  filter(!silencing %in% ("unsilenced"))

plotListQpcrBatchTwo[[2]] <- ggplot(qpcrHFA, aes(gene_abr, pmol.g)) + 
  geom_boxplot(aes(fill = gene_abr), show.legend = FALSE) +
  geom_point(alpha=0.8, colour= gray(0.2), size=2)+
  labs(x="",y=expression(bold("HFA concentration " ~ (pmol ~ g^{-1} ~ "FRW"))))+ 
  presentation+
  #ggpubr::stat_compare_means(comparisons = my_comparisons, method="t.test", size=5) +
  #coord_cartesian(ylim=c(0,2), expand = TRUE)+
  scale_fill_manual(values=cbPaletteC)
t.test(qpcrHFA$pmol.g~qpcrHFA$gene_abr, alternative="two.sided")#t = 0.46565, df = 4.9828, p-value = 0.6611

summary(qpcrHFA$pmol.g)
describeBy(qpcrHFA$pmol.g, group = qpcrHFA$gene_abr)

#    7. save plots in working directory  ---------------------------------------------------
graphics.off()
#Save all plots
saveThisPlotListAsPng(plotListTomatoOne,"TomatoOne")
saveThisPlotListAsPng(plotListTomatoTwo,"TomatoTwo")
saveThisPlotListAsPng(plotListQpcrBatchOne,"QpcrBatchOne")
saveThisPlotListAsPng(plotListQpcrBatchTwo,"QpcrBatchTwo")
graphics.off()







